#常用知识Excel小词典

源表：

![](https://bytebucket.org/tonylua/useful_words_for_web/raw/a9ba039a88ed17be94660f0323364d0e44602e7a/snap-1.png)

按首字母分类：

![](https://bytebucket.org/tonylua/useful_words_for_web/raw/a9ba039a88ed17be94660f0323364d0e44602e7a/snap-2.png)

按领域分类：

![](https://bytebucket.org/tonylua/useful_words_for_web/raw/a9ba039a88ed17be94660f0323364d0e44602e7a/snap-3.png)

## 用法

1. 在 `source_sheet` 工作表中输入或替换数据
2. 在 `scopes_sheet` 工作表中设置对应的分类
3. 分别点击工作表左上方的两个按钮，即可生成对应的归纳表

## 注意事项

- 按钮位置尽量不要移动
- `source_sheet` 和 `scopes_sheet` 不可删除或更名
- 推荐使用 excel 2016 for windows；mac上或其他版本可能会有bug